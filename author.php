<?php
/**
 * The template for displaying Author Archive pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php
	/* Queue the first post, that way we know who
	 * the author is when we try to get their name,
	 * URL, description, avatar, etc.
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if ( have_posts() )
		the_post();
?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title">Articles by <?php echo get_the_author(); ?><?php if(get_the_author_meta('titles')) echo ', ' . get_the_author_meta('titles'); ?></h3>
        
        <div class="category">
        
        <?php if (get_the_author_meta('description')) : ?>
		<div id="authors" class="cf">
        	<div class="about cf">
				<?php if(get_the_author_meta('photo') || get_field('photo',get_the_author_meta('ID'))) : ?>
                <div class="photo">
                	<?php $image = wp_get_attachment_image_src(get_the_author_meta('photo'), 'thumbnail'); ?>
                    <?php if ($image == '') $image = wp_get_attachment_image_src(get_field('photo',get_the_author_meta('ID')), 'thumbnail'); ?>
                	<img src="<?php echo $image[0]; ?>" width="160" height="160" alt="" />
                	<span></span>
                </div>
                <?php endif; ?>
                <div class="bio<?php if(get_the_author_meta('photo') || get_field('photo',get_the_author_meta('ID'))) echo ' narrow'; ?>">
                    <p><?php the_author_meta('description'); ?><?php $our_team_link = get_the_author_meta('our_team_link'); if(($our_team_link[0]) == 'yes') { ?>&nbsp;To learn more about <?php the_author_meta('user_firstname'); ?>, or to contact <?php echo get_the_author_meta('gender'); ?> directly, please see <a href="<?php bloginfo('url'); ?>/about-dream/our-team/">Our Team</a>.<?php } ?></p>
                </div>
            </div>
        </div>
		<?php endif; ?>

		<?php 
			rewind_posts();
            get_template_part( 'loop', 'author' );
        ?>

		</div><!-- .category -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>