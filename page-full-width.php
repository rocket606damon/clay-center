<?php
/**
 * Template Name: Full Width
**/

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content full-width cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page">
			
            <?php the_content(); ?>
            
            <?php comments_template( '', true ); ?>
            
		</div><!-- .page -->
    
    </div><!-- #main -->
    
</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>