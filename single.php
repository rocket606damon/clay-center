<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
        
        <h3 class="title"><?php the_title(); ?></h3>
        
        <div class="article">
        
        	<?php 
			  if(in_category('clay-center-tv')): 
					$field = get_field_object('video_type');
               		$video_type = get_field('video_type');
			  endif;
			  if(in_category('clay-center-tv') && $video_type != 'none'): 
			?>
            <div id="video">
			<?php
                $field = get_field_object('video_type');
                $video_type = get_field('video_type');
                if ($video_type == 'vimeo'):
                    $url = get_field('vimeo_link');
                    $url = str_replace("http:", "", $url);
					$url = str_replace("https:", "", $url);
                    $url = str_replace("www.", "", $url);
                    $url = str_replace("vimeo.com", "", $url);
                    $url = str_replace("/", "", $url);
            ?>
                <iframe src="//player.vimeo.com/video/<?php echo $url; ?>?title=0&amp;byline=0&amp;portrait=0" width="635" height="357" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<?php 
				elseif ($video_type == 'youtube'):
					$url = get_field('youtube_link');
					$url = str_replace("http:", "", $url);
					$url = str_replace("https:", "", $url);
                    $url = str_replace("www.", "", $url);
					$url = str_replace("watch?v=", "", $url);
                    $url = str_replace("youtube.com", "", $url);
					$url = str_replace("youtu.be", "", $url);
                    $url = str_replace("/", "", $url);
			?>
            	<iframe width="635" height="357" src="//www.youtube.com/embed/<?php echo $url; ?>" frameborder="0" allowfullscreen></iframe>
				<?php else: $url = get_field('other'); ?>
				<div id="video_other"><?php echo $url; ?></div>
				<?php endif; ?>
            </div>
        	<?php elseif(has_post_thumbnail()): ?>
            <div id="photo">
            	<?php the_post_thumbnail('article-header'); ?>
                <?php if(get_field('byline')): ?>
               		<div class="byline"><?php the_field('byline'); ?></div>
                <?php endif; ?>
            </div>
			<?php endif; ?>
            <div id="social" class="addthis_toolbox addthis_default_style addthis_32x32_style social">
                <a class="addthis_button_facebook"></a>
                <a class="addthis_button_twitter"></a>
                <a class="addthis_button_google_plusone_share"></a>
                <a class="addthis_button_compact"></a>
                <a class="addthis_counter addthis_bubble_style"></a>
            </div>
			<p class="date"><?php the_time('F j, Y'); ?></p>
        	<?php if(!get_field('hide_author') &&  !in_category('press-releases')){ ?><p class="author">By <?php if ( function_exists( 'coauthors_posts_links' ) ) { coauthors_posts_links(); }   ?></p><?php } ?>
			<p class="cats">Posted in: <?php $categories = get_the_category();$separator = ', ';$output = '';if($categories){foreach($categories as $category) {$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;}echo trim($output, $separator);} ?></p>
            <div class="ccount">
				<?php 
					$myid = $myid = get_the_ID(); $comment_count = $wpdb->get_var("SELECT comment_count FROM $wpdb->posts WHERE post_status = 'publish' AND ID = '$myid'");
					if($comment_count == 1) { 
						echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a><a href="' . get_permalink() . '#disqus_thread">Comment</a>';
					} elseif($comment_count > 1) {
						echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a><a href="' . get_permalink() . '#disqus_thread">Comments</a>';
					}
				?>
            </div>
            
			<?php the_content(); ?>
            
			<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>
            
            <?php /* twentyten_posted_in(); */ ?>
            
           <?php if(!get_field('hide_author') && !in_category('press-releases')){ ?>
            
                <div id="authors" class="cf">
                
                <?php $coauthors = get_coauthors(); ?>
                <?php 
                    foreach( $coauthors as $coauthor ): 
                    $userdata = get_userdata($coauthor->ID);
                    if ($userdata != ''):
                ?>
                    
                    <div class="about cf">
                    
                        <?php if ( $userdata->photo ): ?>
                        <div class="photo">
                            <?php $image = wp_get_attachment_image_src($userdata->photo, 'thumbnail'); ?>
                            <img src="<?php echo $image[0]; ?>" width="160" height="160" alt="<?php echo $userdata->display_name; ?>" />
                            <span></span>
                        </div>
                        <?php endif; ?>
                        
                        <div class="bio<?php if($userdata->photo) echo ' narrow'; ?>">
                            <h3><?php echo $userdata->display_name; if($userdata->titles) echo ', ' . $userdata->titles; ?></h3>
                            <?php
                                $userdata_description = $userdata->user_description;
                                $userdata_string = substr($userdata_description, 0, 200);
                            ?>
                            <p><?php echo $userdata_string . "..."; ?></p>
                            <p><?php if(($userdata->our_team_link[0]) == 'yes') { ?>To learn more about <?php echo $userdata->user_firstname; ?>, or to contact <?php echo $userdata->gender; ?> directly, please see <a href="<?php bloginfo('url'); ?>/about-dream/our-team/#<?php echo $userdata->user_firstname; ?>">Our Team</a>.<?php } ?></p>
                        </div>
                        
                    </div>
                    
                <?php else: ?>
    
                    <div class="about cf">
                        <?php if (get_field('photo',$coauthor->ID)): ?>
                        <div class="photo">
                            <?php $image = wp_get_attachment_image_src(get_field('photo',$coauthor->ID), 'thumbnail'); ?>
                            <img src="<?php echo $image[0]; ?>" width="160" height="160" alt="<?php echo $coauthor->display_name; ?>" />
                            <span></span>
                        </div>
                        <?php endif; ?>
                        
                        <div class="bio<?php if(get_field('photo',$coauthor->ID)) echo ' narrow'; ?>">
                            <h3><?php echo $coauthor->display_name; if($coauthor->titles) echo ', ' . $coauthor->titles; ?></h3>
                            <?php 
                                $coauthor_description = $coauthor->description; 
                                $updated_string = substr($coauthor_description, 0, 250);
                            ?>
                            <p><?php echo $updated_string . "..."; ?></p>
                            <p>To read full bio <a href="<?php bloginfo('url'); ?>/about-us/guest-authors/#<?php echo $coauthor->display_name; ?>">click here</a>.</p>
                        </div>
                        
                    </div>
                      
                <?php endif; ?>
                    
                <?php endforeach; ?>
                
                </div>
            
            <?php } ?>
            
            <?php if(!in_category('press-releases')): ?>
            
				<?php 
     
                $posts = get_field('related_posts');
                 
                if( $posts ): ?>
                    <div id="related">
                        <h3>Related Posts</h3>
                        <ul>
                        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                            <?php setup_postdata($post); ?>
                            <li class="cf">
                                <div class="thumb">
                                    <?php the_post_thumbnail('guest-author-64'); ?>
                                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                </div>
                                <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php else:
                    if ('post' == get_post_type()):
                        $taxs = wp_get_post_tags( $post->ID );
                        if ( $taxs ):
                            $tax_ids = array();
                            foreach( $taxs as $individual_tax ) $tax_ids[] = $individual_tax->term_id;
                             
                            $args = array(
                                'tag__in'               => $tax_ids,
                                'post__not_in'          => array( $post->ID ),
                                'showposts'             => 3,
                                'ignore_sticky_posts'   => 1
                            );
                             
                            $my_query = new wp_query( $args );
                             
                            if( $my_query->have_posts() ) : ?>
                            <div id="related">
                                <h3>Related Posts</h3>
                                <ul>
                                <?php while ($my_query->have_posts()): $my_query->the_post(); ?>
                                
                                    <li class="cf">
                                        <div class="thumb">
                                            <?php the_post_thumbnail('guest-author-64'); ?>
                                            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                        </div>
                                        <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                    </li>
                         
                                <?php endwhile; ?>
                                </ul>
                            </div>
                            <?php endif; wp_reset_query(); ?>
                    <?php endif; endif; ?>
                <?php endif; ?>
            
            <?php endif; ?>

            <!-- <div class="fb-comments" data-href="http://www.mghclaycenter.org/" data-numposts="10"></div> -->
            
			<?php // comments_template( '', true ); ?>

            <?php echo do_shortcode('[fbcomments]'); ?>
            
        </div><!-- .article -->        
        
	</div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>
    
</div><!-- .content -->

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>