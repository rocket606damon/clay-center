<?php
/**
 * Template Name: Contact Information
**/

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page contact-information">
        
        	<?php the_content(); ?>
        
			<div id="map-canvas"><iframe width="635" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=1+Bowdoin+St+%23700,+Boston,+Massachusetts+02114&amp;ie=UTF8&amp;hq=&amp;hnear=1+Bowdoin+St+%23700,+Boston,+Massachusetts+02114&amp;gl=us&amp;t=m&amp;ll=&amp;spn=0.022195,0.054417&amp;z=14&amp;&output=embed&iwloc=near"></iframe></div>
        	
			<ul id="contact" class="cf">
                <li class="email"><a href="mailto:contact@mghclaycenter.org">contact@mghclaycenter.org</a></li>
                <li class="phone">617-643-1590</li>
                <li class="fax">617-724-8690</li>
                <li class="address">One Bowdoin Square, 7th Floor, Boston, MA 02114</li>
                <li class="twitter"><a href="http://twitter.com/mghclaycenter" target="_blank">@MGHClayCenter</a></li>
                <li class="facebook"><a href="http://www.facebook.com/massgeneralclaycenter" target="_blank">/massgeneralclaycenter</a></li>
                <li class="gplus"><a href="https://plus.google.com/101617766762117356957" rel="publisher" target="_blank">The MGH Clay Center for Young Healthy Minds</a></li>
            </ul>
			
			<?php
				$mypages = get_pages( array( 'child_of' => 74, 'sort_column' => 'menu_order') );
				foreach( $mypages as $page ) {		
				$author = get_field('author_link', $page->ID);
				$user_object = get_userdata($author[ID]);
				$titles = get_user_meta($author[ID], 'titles', true );
			?>
            <div class="about cf">
				<?php if(get_user_meta($author[ID], 'photo', true )): ?> 
				<div class="photo">
                	<?php $image = wp_get_attachment_image_src(get_user_meta($author[ID], 'photo', true ), 'thumbnail'); ?>
                	<img src="<?php echo $image[0]; ?>" width="160" height="160" alt="<?php echo get_user_meta($author[ID], 'display_name', true ); ?>" />
                	<span></span>
                </div>
                <?php endif; ?>
                <div class="contact">
                	<h3><?php echo $user_object->display_name; ?>, <?php echo get_user_meta($author[ID], 'titles', true ); ?></h3>
                    <ul class="cf">
                		<li class="email"><a href="mailto:<?php echo $user_object->user_email; ?>"><?php echo $user_object->user_email; ?></a></li>
                    	 <?php if(get_user_meta($author[ID], 'twitter', true ) != '') { ?><li class="twitter"><a href="http://twitter.com/<?php echo get_user_meta($author[ID], 'twitter', true ); ?>" target="_blank"><?php echo get_user_meta($author[ID], 'twitter', true ); ?></a></li><?php } ?>
                	</ul>
                    <a class="btn" href="<?php echo get_page_link( $page->ID ); ?>">View Bio</a>
                    <a class="btn" href="<?php bloginfo('url'); ?>/author/<?php echo $user_object->user_nicename; ?>">View Articles</a>
                </div>
            </div>
			<?php }	?>
			
        
        </div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>