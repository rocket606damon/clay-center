<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title">News &amp; Events</h3>
    
    	<div class="category">

		<?php while ( have_posts() ) : the_post(); ?>

        <div class="block cf">
        	<?php
            	$author = get_field('author'); 
				 $image = wp_get_attachment_image_src(get_user_meta($author[ID], 'photo', true ), 'thumbnail'); 
			?>
            <div class="photo">
            	<img src="<?php echo $image[0]; ?>" width="96" height="96" alt="<?php echo get_user_meta($author[ID], 'display_name', true ); ?>" />
            	<span></span>
            </div>
            <div class="link">
            	<?
					$value = get_field('target');
					if($value[0] == 'true'): 
						$target = "_self";
					else:
						$target = "_blank";
					endif;
				?>
            	<p class="date"><?php the_time('F j, Y'); ?></p>
            	<h3><a href="<?php the_field('url'); ?>" target="<?php echo $target; ?>"><?php the_title(); ?></a></h3>
        	</div>
        </div>
        
    <?php endwhile; // End the loop. Whew. ?>
    
    <?php /* Display navigation to next/previous pages when applicable */ ?>
    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                    <?php next_posts_link( __( '&larr; Older posts', 'twentyten' ) ); ?>
                    <?php previous_posts_link( __( 'Newer posts &rarr;', 'twentyten' ) ); ?>
    <?php endif; ?>

		</div><!-- .category -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>