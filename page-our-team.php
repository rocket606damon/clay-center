<?php
/**
 * Template Name: Our Team
**/

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page our-team">

            <?php
				$mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order') );
				foreach( $mypages as $page ) {		
				$content = $page->post_content;
				$content = apply_filters( 'the_content', $content ); 
				$excerpt = substr($content,0, strpos($content, "</p>")+4);
				$author = get_field('author_link', $page->ID);
				$user_object = get_userdata($author[ID]);
				$titles = get_user_meta($author[ID], 'titles', true );
			?>
            <div class="block cf" id="<?php echo $author['user_firstname']; ?>">
				<?php if(get_the_post_thumbnail($page->ID, 'article-header')): ?> 
				<div class="photo">
					<?php echo get_the_post_thumbnail($page->ID, 'article-header'); ?>
                	<h2 class="first"><?php echo $author['user_firstname']; ?></h2>
                    <h2 class="last"><?php echo $author['user_lastname']; ?>, <?php echo $titles; ?></h2>
                    <ul>
                    	<li><a class="email" href="mailto:<?php echo $user_object->user_email; ?>">Email</a></li>
                         <?php if(get_user_meta($author[ID], 'twitter', true ) != '') { ?><li><a class="twitter" href="http://twitter.com/<?php echo get_user_meta($author[ID], 'twitter', true ); ?>" target="_blank">Twitter</a></li><?php } ?>
                    </ul>
                </div>
                <?php else: ?>
                <h2 class="nophoto"><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a></h2>
                <?php endif; ?>
				<?php echo $excerpt; ?>
                <a class="btn" href="<?php echo get_page_link( $page->ID ); ?>">Continue Reading About <?php echo $author['user_firstname']; ?>...</a>
            </div>
			<?php }	?>
            
            <?php $secondary = get_field('secondary_members'); ?>
             
            <?php if ( $secondary ) : ?>
            
            <div id="authors" class="cf">
                                      
                <?php foreach ($secondary as $user) : 
                    $user_id = $user['ID'];
                ?>
				<div class="about cf">
                	<?php if(get_the_author_meta('photo', $user_id)): ?>
                	<div class="photo">
                    <?php echo wp_get_attachment_image(get_the_author_meta('photo', $user_id)); ?>
                    <span></span>
                    </div>
                    <?php endif; ?>
                    <div class="bio<?php if(get_the_author_meta('photo', $user_id)) echo ' narrow'; ?>">
                	<h3><?php echo get_the_author_meta('display_name', $user_id); ?></h3>
                    <?php echo apply_filters("the_content",get_the_author_meta('description', $user_id)); ?>
                    </div>
                </div> 
             
                <?php endforeach; ?>
            
            </div>
            
            <?php endif; ?>
            
            <?php comments_template( '', true ); ?>
            
		</div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>