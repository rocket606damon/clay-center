<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h3>
    
    	<div class="search">
				
            <?php get_template_part( 'loop', 'search' ); ?>

<?php else : ?>
	
    <div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php _e( 'Nothing Found', 'twentyten' ); ?></h3>
        
        <div class="search">
		
        	<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>

<?php endif; ?>
    
        </div><!-- .search -->

	</div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>
