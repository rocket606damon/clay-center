<?php

get_header(); ?>

<?php
    if( have_posts() )
        the_post();
?>

        <div class="content cf">
        
            <div id="main">
            
                <h3 class="title">All Articles</h3>
            
                <div class="category">
        
                    <?php get_template_part( 'loop', 'index' ); ?>
        
                </div><!-- .category -->
            
            </div><!-- #main -->
            
            <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>
        
        </div><!-- .content -->

<?php get_footer(); ?>