<?php
/**
 * Template Name: Guest Authors
**/

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page guest-authors">

			<div id="authors" class="cf">
            
				<?php 
                $args = array( 'post_type' => 'guest-author', 'numberposts' => -1, 'orderby' => 'meta_value', 'order' => 'ASC', 'meta_key' =>'cap-last_name');
                $guest_authors = get_posts( $args );
                foreach( $guest_authors as $ga ): 
                $author = get_post_meta($ga->ID);
                ?>
                
                <div class="about cf" id="<?php echo get_post_meta($ga->ID, 'cap-display_name', true);?>">
                    <?php if(wp_get_attachment_image(get_post_meta($ga->ID, 'photo', true))): ?>
                    <div class="photo">
                    <?php echo wp_get_attachment_image(get_post_meta($ga->ID, 'photo', true)); ?>
                    <span></span>
                    </div>
                    <?php endif; ?>
                    <div class="bio<?php if(wp_get_attachment_image(get_post_meta($ga->ID, 'photo', true))) echo ' narrow'; ?>">
                    <h3><?php echo get_post_meta($ga->ID, 'cap-display_name', true); ?></h3>
                    <p><?php echo get_post_meta($ga->ID, 'cap-description', true); ?></p>
                    <a class="btn" href="<?php echo bloginfo('url') . '/author/' . get_post_meta($ga->ID, 'cap-user_login', true); ?>">View Articles</a>
                	</div>
                </div>
                <?php endforeach; ?>
            
			</div>
            
            <?php comments_template( '', true ); ?>
        
        </div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>