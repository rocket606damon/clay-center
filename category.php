<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php 
	$this_category = get_category($cat);
	$category_link = get_category_link($this_category->ID);
	if ($this_category->category_nicename == 'parenting-concerns'):
?>

<?php include( TEMPLATEPATH . '/includes/parenting-concerns.php'); ?>

<?php else: ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php echo single_cat_title( '', false ); ?><span><a href="<?php $this_category = get_category($cat); echo get_category_link($this_category->ID); ?>feed/" target="_blank"><i class="fa fa-rss-square"></i></a></span></h3>
    
    	<div class="category">
        
        	<?php if(is_category('patriots-day-project')): ?>

                <div id="patriots-day">
                
                    <p>In response to the one-year anniversary of the Boston Marathon bombing in April 2014, the Marjorie E. Korff Parenting At a Challenging Time (PACT) Program at Massachusetts General Hospital, in collaboration with The Clay Center for Young Healthy Minds, has launched the Patriots' Day Project.</p>
                    
                    <p>This project creates educational tools reflecting the lessons learned from the bombing and its aftermath, resources intended to assist parents and educators as they face current and future challenges that impact the children in our communities. These materials blend the shared wisdom of approximately 400 parents from Boston and surrounding communities, as well as MGH experts in child development and emotional health.</p>
                    
                    <p>We invite you to explore the blogs, podcasts and original video that will be released regularly as the 2014 Boston Marathon approaches. To be sure that you are up to date on the latest content, feel free to check back often and follow/“Like” us on Twitter and Facebook. And, if you wish to learn more about the services and support the Marjorie E. Korff PACT Program provides to families coping with serious illness, visit <a href="http://www.mghpact.org" target="_blank">http://www.mghpact.org</a>.</p>
                
                </div>
                
            <?php endif; ?>

			<?php get_template_part( 'loop', 'category' ); ?>

		</div><!-- .category -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>

<?php endif; ?>