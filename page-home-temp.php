<?php
/**
 * Template Name: Home
**/

get_header(); ?>

<div id="blocks">

	<div class="content">
    
		<?php 
			if(get_field('featured_faculty')):
			$faculty = get_field('faculty_member');
			$count = count($faculty);
			$i = rand(0, $count - 1);
			$image = wp_get_attachment_image_src($faculty[$i]['faculty_image'], 'full'); 
			$excerpt = $faculty[$i]['faculty_excerpt'];
		?>
        <div class="block block_left" style="background-image: url(<?php echo $image[0]; ?>);">
            <h3>Featured Faculty</h3>
            <?php
                $post_object = $faculty[$i]['faculty_link'];
                if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); 
            ?>
            <h4><a href="<?php the_permalink() ?>"><?php if($name != '') { echo $name; } else { the_title(); } ?></a></h4>
            <?php if($excerpt != '') { echo '<p>' . $excerpt . '</p>'; } else { the_excerpt(); } ?>
            <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
        <?php else: ?>
        <?php $image = wp_get_attachment_image_src(get_field('image_left'), 'full'); ?>
        <div class="block block_left" style="background-image: url(<?php echo $image[0]; ?>);">
            <h3><?php the_field('title_left'); ?></h3>
            <h4><a href="<?php the_field('link_left'); ?>"><?php the_field('subtitle_left'); ?></a></h4>
            <?php echo '<p>' . get_field('excerpt_left') . '</p>'; ?>
            <?php if(get_field('lightbox_left')): ?>
            <a class="btn lightbox" href="#<?php the_field('lightbox_slug_left'); ?>" data-target="#<?php the_field('lightbox_slug_left'); ?>"><?php the_field('button_text_left'); ?></a>
            <?php else: ?>
            <a class="btn" href="<?php the_field('link_left'); ?>"><?php the_field('button_text_left'); ?></a>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        
        <?php $image = wp_get_attachment_image_src(get_field('image_center'), 'full'); ?>
        <div class="block block_center" style="background-image: url(<?php echo $image[0]; ?>);">
            <h3><?php the_field('title_center'); ?></h3>
            <h4><a href="<?php the_field('link_center'); ?>"><?php the_field('subtitle_center'); ?></a></h4>
            <?php echo '<p>' . get_field('excerpt_center') . '</p>'; ?>
            <?php if(get_field('lightbox_center')): ?>
            <a class="btn lightbox" href="#<?php the_field('lightbox_slug_center'); ?>" data-target="#<?php the_field('lightbox_slug_center'); ?>"><?php the_field('button_text_center'); ?></a>
            <?php else: ?>
            <a class="btn" href="<?php the_field('link_center'); ?>"><?php the_field('button_text_center'); ?></a>
            <?php endif; ?>
        </div>
		
       	<?php $image = wp_get_attachment_image_src(get_field('image_right'), 'full'); ?>
        <div class="block block_right" style="background-image: url(<?php echo $image[0]; ?>);">
            <h3><?php the_field('title_right'); ?></h3>
            <h4><a href="<?php the_field('link_right'); ?>"><?php the_field('subtitle_right'); ?></a></h4>
            <?php echo '<p>' . get_field('excerpt_right') . '</p>'; ?>
            <?php if(get_field('lightbox_right')): ?>
            <a class="btn lightbox" href="#<?php the_field('lightbox_slug_right'); ?>" data-target="#<?php the_field('lightbox_slug_right'); ?>"><?php the_field('button_text_right'); ?></a>
            <?php else: ?>
            <a class="btn" href="<?php the_field('link_right'); ?>"><?php the_field('button_text_right'); ?></a>
            <?php endif; ?>
        </div>
	
    </div><!-- .content -->    

</div><!-- #blocks -->

<div class="content cf">

	<div id="main">
        <h3 class="title">Up to Date Articles &amp; Stories</h3>
        <div id="tabs">
        
            <ul class="cf">
            	<li><a href="#latest">Latest Content</a></li>
                <li><a href="#hot">Hot Topics</a></li>
                <li><a href="#trending">Trending Articles</a></li>
                <li><a href="#media">Multimedia</a></li>
            </ul>
            
            <div id="latest">
            	
               <?php  /* NEED TO PUT THIS IN THE LOOP */
                    $latest = new WP_Query('cat=-3924&showposts=6'); 
                    if ( $latest->have_posts() ) : 
                    while ( $latest->have_posts() ) : $latest->the_post();
               ?>
                
                <div class="block cf">
                	<?php if(has_post_thumbnail()): ?>
                    <div class="photo">
                        <?php the_post_thumbnail('thumbnail'); ?>
                        <a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>
                    </div>
                    <?php endif; ?>
                    <div class="text<?php if(has_post_thumbnail()) echo ' narrow'; ?>">
                        <p class="date"><?php the_time('F j, Y'); ?></p>
                        <?php
                            $category = get_the_category();
                            if ($category) { echo '<a class="cat" href="' . get_category_link( $category[0]->term_id ) . '" title="' . $category[0]->name . '" ' . '>' . $category[0]->name.'</a> '; }
                        ?>			
                        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <?php print_excerpt(200); ?>
                        <?php if(in_category('podcasts')): ?>
                        <a class="media-cat podcasts" href="<?php bloginfo('url'); ?>/multimedia/podcasts/">Podcasts</a>
                        <?php elseif(in_category('clay-center-tv')): ?>
                        <a class="media-cat clay-center-tv" href="<?php bloginfo('url'); ?>/multimedia/clay-center-tv/">Clay Center TV</a>
                        <?php endif; ?>
                        <a class="btn" href="<?php the_permalink() ?>">View More...</a>
                        <?php $myid = $hot->post->ID; $comment_count = $wpdb->get_var("SELECT comment_count FROM $wpdb->posts WHERE post_status = 'publish' AND ID = '$myid'");
						if($comment_count > 0) echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a>'; ?>
                	</div>
                </div>
				
				
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
                <?php endif;?>
            
            </div><!-- #latest -->
            
            <div id="hot">
           
				<?php  /* NEED TO PUT THIS IN THE LOOP */
                    $hot = new WP_Query('cat=13&showposts=6'); 
                    if ( $hot->have_posts() ) : 
                    while ( $hot->have_posts() ) : $hot->the_post(); 
				?>
                
                <div class="block cf">
                	<?php if(has_post_thumbnail()): ?>
                    <div class="photo">
                        <?php the_post_thumbnail('thumbnail'); ?>
                        <a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>
                    </div>
                    <?php endif; ?>
                    <div class="text<?php if(has_post_thumbnail()) echo ' narrow'; ?>">
                        <p class="date"><?php the_time('F j, Y'); ?></p>
                        <a title="Hot Topics" href="<?php bloginfo('url'); ?>/hot-topics/" class="cat">Hot Topics</a>
                        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <?php print_excerpt(200); ?>
                        <?php if(in_category('podcasts')): ?>
                        <a class="media-cat podcasts" href="<?php bloginfo('url'); ?>/multimedia/podcasts/">Podcasts</a>
                        <?php elseif(in_category('clay-center-tv')): ?>
                        <a class="media-cat clay-center-tv" href="<?php bloginfo('url'); ?>/multimedia/clay-center-tv/">Clay Center TV</a>
                        <?php endif; ?>
                        <a class="btn" href="<?php the_permalink() ?>">View More...</a>
                        <?php $myid = $hot->post->ID; $comment_count = $wpdb->get_var("SELECT comment_count FROM $wpdb->posts WHERE post_status = 'publish' AND ID = '$myid'");
						if($comment_count > 0) echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a>'; ?>
                	</div>
                </div>
				
				
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
                <?php endif;?>
           
            </div><!-- #hot -->
            
            <div id="trending">
            
            	<?php  /* NEED TO PUT THIS IN THE LOOP */
                    $trending = new WP_Query('showposts=6&orderby=rand&cat=-3924'); 
                    if ( $trending->have_posts() ) : 
                    while ( $trending->have_posts() ) : $trending->the_post();
                ?>
                
                <div class="block cf">
                	<?php if(has_post_thumbnail()): ?>
                    <div class="photo">
                        <?php the_post_thumbnail('thumbnail'); ?>
                        <a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>
                    </div>
                    <?php endif; ?>
                    <div class="text<?php if(has_post_thumbnail()) echo ' narrow'; ?>">
                        <p class="date"><?php the_time('F j, Y'); ?></p>
                        <?php
                            $category = get_the_category();
                            if ($category) { echo '<a class="cat" href="' . get_category_link( $category[0]->term_id ) . '" title="' . $category[0]->name . '" ' . '>' . $category[0]->name.'</a> '; }
                        ?>			
                        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <?php print_excerpt(200); ?>
                        <?php if(in_category('podcasts')): ?>
                        <a class="media-cat podcasts" href="<?php bloginfo('url'); ?>/multimedia/podcasts/">Podcasts</a>
                        <?php elseif(in_category('clay-center-tv')): ?>
                        <a class="media-cat clay-center-tv" href="<?php bloginfo('url'); ?>/multimedia/clay-center-tv/">Clay Center TV</a>
                        <?php endif; ?>
                        <a class="btn" href="<?php the_permalink() ?>">View More...</a>
                        <?php $myid = $hot->post->ID; $comment_count = $wpdb->get_var("SELECT comment_count FROM $wpdb->posts WHERE post_status = 'publish' AND ID = '$myid'");
						if($comment_count > 0) echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a>'; ?>
                	</div>
                </div>
				
				
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
                <?php endif;?>
            
            </div><!-- #trending -->
            
             <div id="media">
           
				<?php  /* NEED TO PUT THIS IN THE LOOP */
                    $media = new WP_Query('cat=3454,3455&showposts=6'); 
                    if ( $media->have_posts() ) : 
                    while ( $media->have_posts() ) : $media->the_post(); 
				?>
                
                <div class="block cf">
                	<?php if(has_post_thumbnail()): ?>
                    <div class="photo">
                        <?php the_post_thumbnail('thumbnail'); ?>
                        <a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>
                    </div>
                    <?php endif; ?>
                    <div class="text<?php if(has_post_thumbnail()) echo ' narrow'; ?>">
                        <p class="date"><?php the_time('F j, Y'); ?></p>
                        <?php  if (in_category('clay-center-tv')): ?>
						<a class="cat" href="<?php bloginfo('url'); ?>/multimedia/clay-center-tv/" title="Clay Center TV">Clay Center TV</a>
						<?php else: ?>
						<a class="cat" href="<?php bloginfo('url'); ?>/multimedia/podcasts/" title="videos">Podcasts</a> 
						<?php endif; ?>			
                        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <?php print_excerpt(200); ?>
                        <?php if(in_category('podcasts')): ?>
                        <a class="media-cat podcasts" href="<?php bloginfo('url'); ?>/multimedia/podcasts/">Podcasts</a>
                        <?php elseif(in_category('clay-center-tv')): ?>
                        <a class="media-cat clay-center-tv" href="<?php bloginfo('url'); ?>/multimedia/clay-center-tv/">Clay Center TV</a>
                        <?php endif; ?>
                        <a class="btn" href="<?php the_permalink() ?>">View More...</a>
                        <?php $myid = $media->post->ID; $comment_count = $wpdb->get_var("SELECT comment_count FROM $wpdb->posts WHERE post_status = 'publish' AND ID = '$myid'");
						if($comment_count > 0) echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a>'; ?>
                	</div>
                </div>
				
				
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
                <?php endif;?>
           
            </div><!-- #media -->
                        
		</div><!-- #tabs -->

     </div><!-- #main -->
        
     <?php include( TEMPLATEPATH . '/includes/sidebar-home.php'); ?>

</div>

<?php if(get_field('lightbox_left')): ?>
<div id="<?php the_field('lightbox_slug_left'); ?>" style="display:none;">
    <?php the_field('lightbox_content_left'); ?>
</div>
<?php endif; ?>

<?php if(get_field('lightbox_center')): ?>
<div id="<?php the_field('lightbox_slug_center'); ?>" style="display:none;">
    <?php the_field('lightbox_content_center'); ?>
</div>
<?php endif; ?>

<?php if(get_field('lightbox_right')): ?>
<div id="<?php the_field('lightbox_slug_right'); ?>" style="display:none;">
    <?php the_field('lightbox_content_right'); ?>
</div>
<?php endif; ?>

<?php get_footer(); ?>