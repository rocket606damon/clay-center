<?php
/**
 * Template Name: Contact Information
**/

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">

    	<h3 class="title"><?php the_title(); ?></h3>

    	<div class="page contact-information">

        <?php the_content(); ?>

				<?php if(get_field('embed_google_map')): ?><div id="map-canvas"><?php the_field('embed_google_map'); ?></div><?php endif; ?>

				<ul id="contact" class="cf">
	          <?php if(get_field('email')): ?><li class="email"><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></li><?php endif; ?>
	          <?php if(get_field('phone')): ?><li class="phone"><?php the_field('phone'); ?></li><?php endif; ?>
	        	<?php if(get_field('fax')): ?>  <li class="fax"><?php the_field('fax'); ?></li><?php endif; ?>
	          <?php if(get_field('address')): ?><li class="address"><?php the_field('address'); ?></li><?php endif; ?>
	          <?php if(get_field('twitter_link') && get_field('twitter_text')): ?><li class="twitter"><a href="<?php the_field('twitter_link'); ?>" target="_blank"><?php the_field('twitter_text'); ?></a></li><?php endif; ?>
	          <?php if(get_field('facebook_link') && get_field('facebook_text')): ?><li class="facebook"><a href="<?php the_field('facebook_link'); ?>" target="_blank"><?php the_field('facebook_text'); ?></a></li><?php endif; ?>
	      </ul>

				<?php
					$mypages = get_pages( array( 'child_of' => 74, 'sort_column' => 'menu_order') );
					foreach( $mypages as $page ) {
					$author = get_field('author_link', $page->ID);
					$user_object = get_userdata($author[ID]);
					$titles = get_user_meta($author[ID], 'titles', true );
				?>
            <div class="about cf">
				<?php if(get_user_meta($author[ID], 'photo', true )): ?>
				<div class="photo">
                	<?php $image = wp_get_attachment_image_src(get_user_meta($author[ID], 'photo', true ), 'thumbnail'); ?>
                	<img src="<?php echo $image[0]; ?>" width="160" height="160" alt="<?php echo get_user_meta($author[ID], 'display_name', true ); ?>" />
                	<span></span>
                </div>
                <?php endif; ?>
                <div class="contact">
                	<h3><?php echo $user_object->display_name; ?>, <?php echo get_user_meta($author[ID], 'titles', true ); ?></h3>
                    <ul class="cf">
                		<li class="email"><a href="mailto:<?php echo $user_object->user_email; ?>"><?php echo $user_object->user_email; ?></a></li>
                    	 <?php if(get_user_meta($author[ID], 'twitter', true ) != '') { ?><li class="twitter"><a href="http://twitter.com/<?php echo get_user_meta($author[ID], 'twitter', true ); ?>" target="_blank"><?php echo get_user_meta($author[ID], 'twitter', true ); ?></a></li><?php } ?>
                	</ul>
                    <a class="btn" href="<?php echo get_page_link( $page->ID ); ?>">View Bio</a>
                    <a class="btn" href="<?php bloginfo('url'); ?>/author/<?php echo $user_object->user_nicename; ?>">View Articles</a>
                </div>
            </div>
			<?php }	?>


        </div><!-- .page -->

    </div><!-- #main -->

    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>