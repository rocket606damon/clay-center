<div id="sidebar">
    
    <?php if(is_404()): ?>
    <div id="search" class="block cf">
    	<h3 class="title">Search</h3>
       	<form role="search" method="get" class="searchform-2" action="<?php echo home_url( '/' ); ?>">
            <input type="text" placeholder="What’s on your mind?" name="s" id="s" />
            <input type="submit" id="searchsubmit" value="Search" />
        </form>
    </div>
    <?php endif; ?>
    
    <?php if((is_category('patriots-day-project') || in_category('patriots-day-project')) && !is_category('videos') && !is_category('podcasts')): ?>
    
    <div id="newsletter" class="block cf">
        <h3 class="title">Newsletter Sign-Up</h3>
        <p>Your monthly dose of the latest mental health tips and advice from our team</p>
        <div class="side">
        <?php echo do_shortcode("[gravityform id='2' name='Newsletter' title='false' description='false' ajax='true']"); ?>
        </div>
    </div>

    <div id="supported" class="block cf">
    	<h3 class="title">Supported By</h3>
        <div class="side">
       		<p>The Patriots' Day Project is supported through a generous gift from the employees at Fidelity Investments.</p>
    	</div>
    </div>
    
    <div id="social" class="block cf">
    	<h3 class="title">Stay Informed</h3>
        <div class="side">
        	<p>Like, follow, and subscribe to stay up to date on current events and news from The Clay Center.</p>
            <ul>
            	<li><a class="facebook" title="Join Us On Facebook" href="http://www.facebook.com/massgeneralclaycenter" target="_blank"><i class="fa fa-facebook fa-2x"></i></a></li>
                <li><a class="twitter" title="Follow Us On Twitter" href="http://twitter.com/mghclaycenter" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
                <li><a class="gplus" title="Join Us On Google+" href="https://plus.google.com/101617766762117356957" rel="publisher" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a></li>
                <li><a class="youtube" title="Watch us on YouTube" href="https://www.youtube.com/channel/UCBdGAoFma-eEZL4ywneftIg" target="_blank"><i class="fa fa-youtube fa-2x"></i></a></li>
            </ul>
    	</div>
    </div>
    
    <?php endif; ?>
    
    <?php if(in_category('press-releases') && !is_search() &&!is_404()): ?>

    <div id="newsletter" class="block cf">
        <h3 class="title">Newsletter Sign-Up</h3>
        <p>Your monthly dose of the latest mental health tips and advice from our team</p>
        <div class="side">
        <?php echo do_shortcode("[gravityform id='2' name='Newsletter' title='false' description='false' ajax='true']"); ?>
        </div>
    </div> 

    <div id="subnav" class="block cf">
    	<h3 class="title">Explore This Section</h3>
        <?php 
			$children = wp_list_pages("sort_column=menu_order&title_li=&child_of=5502&echo=0&depth=4"); 
			$children = str_replace("<li class=\"page_item page-item-5609\">", "<li class=\"page_item page-item-5609 current_page_item\">", $children);
		?>
        <ul>
       	<?php echo $children; ?>
		</ul>
    </div>
	<?php endif; ?>
    
	<?php
	
	$parent = array_reverse(get_post_ancestors($post->ID));
	$first_parent = get_page($parent[0]);
	$children = get_pages('child_of='.$first_parent->ID); 
	if((count( $children ) != 0 ) && !is_search() &&!is_404() && $post->post_parent): ?>
	<div id="subnav" class="block cf">
    	<h3 class="title">Explore This Section</h3>
        	<?php
			  $children = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$first_parent->ID."&echo=0&depth=4");
			  if ($children) { ?>
			  <ul>
			  <?php echo $children; ?>
			  </ul>
			 <?php } ?>
    </div>
    
    <?php elseif((!is_category() &&  !is_single() && !is_author())): ?>
    
    <div id="newsletter" class="block cf">
        <h3 class="title">Newsletter Sign-Up</h3>
        <p>Your monthly dose of the latest mental health tips and advice from our team</p>
        <div class="side">
        <?php echo do_shortcode("[gravityform id='2' name='Newsletter' title='false' description='false' ajax='true']"); ?>
        </div>
    </div>

	<div id="social" class="block cf">
    	<h3 class="title">Stay Informed</h3>
        <div class="side">
        	<p>Like, follow, and subscribe to stay up to date on current events and news from The Clay Center.</p>
            <ul>
            	<li><a class="facebook" title="Join Us On Facebook" href="http://www.facebook.com/massgeneralclaycenter" target="_blank"><i class="fa fa-facebook fa-2x"></i></a></li>
                <li><a class="twitter" title="Follow Us On Twitter" href="http://twitter.com/mghclaycenter" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
                <li><a class="gplus" title="Join Us On Google+" href="https://plus.google.com/101617766762117356957" rel="publisher" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a></li>
                <li><a class="youtube" title="Watch us on YouTube" href="https://www.youtube.com/channel/UCBdGAoFma-eEZL4ywneftIg" target="_blank"><i class="fa fa-youtube fa-2x"></i></a></li>
            </ul>
    	</div>
    </div>
    
	<?php endif; ?>
    
    <?php if(is_post_type_archive('base_news_events')): ?>
    <div id="archive" class="block cf">
    	<h3 class="title">Archive</h3>
        <ul>
       		<?php get_cpt_archives('base_news_events', true); ?>
    	</ul>
    </div>
   <?php endif; ?>
    
    <?php if((is_category() || is_single() || is_author() || is_tag()) && (!is_category('patriots-day-project') || !in_category('patriots-day-project')) && !is_category('press-releases') && !in_category('press-releases')): ?>

    <div id="newsletter" class="block cf">
        <h3 class="title">Newsletter Sign-Up</h3>
        <p>Your monthly dose of the latest mental health tips and advice from our team</p>
        <div class="side">
        <?php echo do_shortcode("[gravityform id='2' name='Newsletter' title='false' description='false' ajax='true']"); ?>
        </div>
    </div>

    <div id="multimedia" class="block cf">
        <h3 class="title">Multimedia</h3>
        <ul>
            <?php 
                $args=array(
                    'child_of' => 3453,
                    'orderby' => 'ID',
                    'hide_empty' => 0,
                    'order' => 'ASC'
                );
                $categories=get_categories($args);
                foreach($categories as $category) { 
                    echo '<li class="' . $category->slug .'"><a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name.'</a></li> ';
                } 
            ?>  
        </ul>
    </div>
    
    <div id="quickjump" class="block cf">
        <h3 class="title">Quick Jump</h3>
        <ul>
      <?php 
        $categories = get_terms('category', array(
          'include' => array('13','6','7','8','9'),
          'orderby' => 'include'
        ));

        foreach ($categories as $category){
          if($category->term_id == '13'){
            echo '<li class="' . $category->slug .'"><a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name.'</a></li><li class="news-events"><a href="' . home_url() . '/news-events/" title="News & Events">News &amp; Events</a></li>';
          } else{
            echo '<li class="' . $category->slug .'"><a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name.'</a></li> ';
          }
        }
      ?>
        </ul>
    </div>

    <div id="cloud" class="block cf">
    	<h3 class="title">Tag Cloud</h3>
        <div class="side">
     		<?php wp_tag_cloud(); ?> 
    	</div>
    </div>

    <?php endif; ?>
    
</div>