<div id="sidebar">

	<div id="social" class="block cf">
    	<h3 class="title">Stay Informed</h3>
        <div class="side">
        	<p>Like, follow, and subscribe to stay up to date on current events and news from The Clay Center.</p>
            <ul>
            	<li><a class="facebook" title="Join Us On Facebook" href="http://www.facebook.com/massgeneralclaycenter" target="_blank"><i class="fa fa-facebook fa-2x"></i></a></li>
                <li><a class="twitter" title="Follow Us On Twitter" href="http://twitter.com/mghclaycenter" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
                <li><a class="youtube" title="Watch us on YouTube" href="https://www.youtube.com/channel/UCBdGAoFma-eEZL4ywneftIg" target="_blank"><i class="fa fa-youtube fa-2x"></i></a></li>
            </ul>
    	</div>
    </div>

    <div id="newsletter" class="block cf">
    	<h3 class="title">Newsletter Sign-Up</h3>
    	<div class="side">
    	<?php echo do_shortcode("[gravityform id='2' name='Newsletter' title='false' description='false' ajax='true']"); ?>
		</div>
    </div>

    <?php if(get_field('select_poll', 'option') && (get_field('poll_status', 'option') == 'active')): ?>
    	<?php $form = get_field('select_poll', 'option'); ?>
        <div id="poll" class="block cf">
            <h3 class="title"><?php the_field('title', 'option'); ?></h3>
            <div class="side">
							<?php
								//echo '<pre>';
								//print_r($form);
								//echo '</pre>';
								gravity_form_enqueue_scripts($form['id'], true);
								gravity_form($form['id'], false, false, false, '', true, 1);
							?>
    				</div>
        </div>
	<?php endif; ?>

    <?php if(get_field('video_embed_link', 'option') && (get_field('video_status', 'option') == 'active')): ?>
    <div id="video" class="block cf" style="display: none;">
    	<h3 class="title">Featured Video</h3>
        <div class="side">
        	<p><?php the_field('description', 'option'); ?></p>
            <?php the_field('video_embed_link', 'option'); ?>
        </div>
    </div>
    <?php endif; ?>

    <div id="multimedia" class="block cf">
        <h3 class="title">Multimedia</h3>
        <ul>
            <?php
                $args=array(
                    'child_of' => 3453,
                    'orderby' => 'ID',
                    'hide_empty' => 0,
                    'order' => 'ASC'
                );
                $categories=get_categories($args);
                foreach($categories as $category) {
                    echo '<li class="' . $category->slug .'"><a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name.'</a></li> ';
                }
            ?>
        </ul>
    </div>

    <div id="quickjump" class="block cf">
    	<h3 class="title">Quick Jump</h3>
        <ul>
      <?php 
        $categories = get_terms('category', array(
          'include' => array('13','6','7','8','9'),
          'orderby' => 'include'
        ));

        foreach ($categories as $category){
          if($category->term_id == '13'){
            echo '<li class="' . $category->slug .'"><a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name.'</a></li><li class="news-events"><a href="' . home_url() . '/news-events/" title="News & Events">News &amp; Events</a></li>';
          } else{
            echo '<li class="' . $category->slug .'"><a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name.'</a></li> ';
          }
        }
      ?>
        </ul>
    </div>

</div>
