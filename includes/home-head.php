<?php 
$count = get_field('slider');
if(count($count)>1): ?>
<script src="<?php bloginfo('url'); ?>/js/cfs.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('url'); ?>/js/ui.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('url'); ?>/js/home.js" type="text/javascript"></script>
<script src="<?php bloginfo('url'); ?>/js/search.js" type="text/javascript"></script>
<?php endif; ?>
<?php if(get_field('lightbox_left') || get_field('lightbox_center') || get_field('lightbox_right')): ?>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('url'); ?>/js/fb/jquery.fancybox.css" />
<script src="<?php bloginfo('url'); ?>/js/fb/jquery.fancybox.js" type="text/javascript"></script>
<script type="text/javascript">$(document).ready(function() {$(".lightbox").fancybox();});</script>
<?php endif; ?>