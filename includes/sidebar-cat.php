<div id="sidebar">
    
    <div id="newsletter" class="block cf">
        <h3 class="title">Newsletter Sign-Up</h3>
        <p>Your monthly dose of the latest mental health tips and advice from our team</p>
        <div class="side">
        <?php echo do_shortcode("[gravityform id='2' name='Newsletter' title='false' description='false' ajax='true']"); ?>
        </div>
    </div>
    
    <div id="search" class="block cf">
    	<h3 class="title">Search</h3>
       	<form role="search" method="get" class="searchform-2" action="<?php echo home_url( '/' ); ?>">
            <input type="text" placeholder="What’s on your mind?" name="s" id="s" />
            <input type="submit" id="searchsubmit" value="Search" />
        </form>
    </div>
    
    <div id="latest" class="block cf">
    	<h3 class="title">Latest Posts</h3>
        <div class="side">
       	<ul>
		<?php
        	$args = array( 'numberposts' => '5','post_status' => 'publish' );
            $recent_posts = wp_get_recent_posts( $args );
            foreach( $recent_posts as $recent ){
                echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
            }
        ?>
        </ul>
        </div>
    </div>

    <div id="cloud" class="block cf">
    	<h3 class="title">Tag Cloud</h3>
        <div class="side">
     		<?php wp_tag_cloud(); ?> 
    	</div>
    </div>
    
</div>