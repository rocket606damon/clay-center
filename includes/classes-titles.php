<?php 
if(is_category()){
    $category = get_category($cat);
    $page_class = $category->slug;
    if ($category->cat_ID == 9){
        $category_title = 'You &amp; Your Family';
        $category_desc = category_description(9);
	} elseif ($category->cat_ID == 3454){
		$category_title = $category->name;
        $category_desc = category_description(3454);
	} elseif ($category->cat_ID == 3455){
		$category_title = $category->name;
        $category_desc = category_description(3455);
    } elseif ($category->parent != 0) {
        $category_title = 'Parenting Concerns';
        $category_desc = category_description(1);
    } else {
        $category_title = $category->name;
        $category_desc = $category->category_description;
    }
} elseif(is_single()) {
    $categories = get_the_category();
    if(in_category('multimedia')){
        $page_class = 'multimedia';
    }
    elseif (in_category('hot-topics')) {
        $page_class = 'hot-topics';
	} elseif(in_category('patriots-day-project')) {
        $page_class = 'patriots-day-project';
	} elseif(in_category('press-releases')) {
    } elseif(in_category('families')) {
        $page_class = 'families';
    } else {
        $page_class = "parenting-concerns";
    }
    $category = get_term_by('slug',$page_class,'category');
    $category_title = $category->name;
    $category_desc = category_description(get_category_by_slug($page_class)->term_id);
} elseif(is_archive() && !is_author() && !is_tag()){
    $page_class = 'news-events';
    $category_title = 'News &amp; Events';
    $category_desc = 'We keep pretty busy here at The Clay Center! Check out some of our team’s notable media appearances and contributions below.';
} elseif(is_search() || is_tag()){
    $page_class = 'page-search';
    $category_title = 'Search Results';
    $category_desc = 'Let’s get you to the right place.';
} elseif(is_404()){
    $page_class = 'page-not-found';
    $category_title = 'Not Found';
    $category_desc = 'Oops! We can\'t seem to find that page!';
} elseif(is_author()){
    $page_class = 'about-us';
    $category_title =  get_field('section_title',12);
    $category_desc = get_field('section_description',12);
} elseif(is_page('all-articles')) {
    $page_class = 'parenting-concerns';
    $category_title = single_cat_title(1);
    $category_desc = category_description(1);
} elseif(is_page()) { 
    if ($post->post_parent)	{
        $ancestors=get_post_ancestors($post->ID);
        $root=count($ancestors)-1;
        $parent = $ancestors[$root];
    } else {
        $parent = $post->ID;
    } 
    $page = get_post($parent);
    $page_class = $page->post_name;
    if(get_field('section_title',$page->ID)){
        $category_title = get_field('section_title',$page->ID);
    } else {
        $category_title = $page->post_title;
    }
    $category_desc = get_field('section_description',$page->ID);
} else {
    $page_class = $post->post_name;
    if(get_field('section_title',$post->ID)){
        $category_title = get_field('section_title',$post->ID);
    } else {
        $category_title = $page->post_title;
    }
    $category_desc = get_field('section_description',$post->ID);
}
?>