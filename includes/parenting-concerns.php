<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php echo $this_category->cat_name; ?><span><a href="<?php $this_category = get_category($cat); echo get_category_link($this_category->ID); ?>feed/" target="_blank"><i class="fa fa-rss-square"></i></a></span></h3>
    
    	<div class="category">

            <ul class="menu">
			<?php 
                $args=array(
                    'child_of' => 1,
                    'orderby' => 'ID',
                    'hide_empty' => 0,
                    //'exclude' => '4362,13',
                    'order' => 'ASC'
                );
                $categories=get_categories($args);
                foreach($categories as $category) { 
                    echo '<li class="' . $category->slug .'">';
					$name = str_replace("-", "<br />", $category->name);
					echo '<h3>' . $name . '</h3>';
					echo '<a class="btn" href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>View</a>';
                	echo '</li>';
				} 
            ?>  
            </ul>
            
            <div class="center"><a href="../all-articles/" class="btn">View All Articles</a></div>
            
        </div><!-- .category -->
        
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar-cat.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>