<div id="slider">   
<?php if(get_field('banner')): ?>
<?php while(the_repeater_field('banner')): ?>  
<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'full'); ?>
<div class="wrap" style="background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url(<?php echo $image[0]; ?>) top center no-repeat;">
				
<?php
$post_object = get_sub_field('page');
	$post = $post_object;
	setup_postdata( $post ); 
?>
				
  <div class="content">        
    <div class="message">
      <h2><a href="<?php if(get_sub_field('custom_url') != '') { the_sub_field('custom_url'); } else { the_permalink(); } ?>"><?php if(get_sub_field('custom_title') != '') { the_sub_field('custom_title'); } else { the_title(); } ?></a></h2>
      <?php if(get_sub_field('blurb') != '') { echo '<p>' . get_sub_field('blurb') . '</p>'; } else { print_excerpt(150); } ?>
      <div class="btn-placement">
      <a class="btn" href="<?php if(get_sub_field('custom_url') != '') { the_sub_field('custom_url'); } else { the_permalink(); } ?>"><?php if(get_sub_field('button_text') != ''){ the_sub_field('button_text'); } ?></a>
      </div>
      <!-- <div class="bar"><div class="timer"></div></div> -->
      <div class="prev"></div>
      <div class="pause"></div>
      <div class="next"></div>
    </div>
<?php wp_reset_postdata(); ?>
                        
  </div>
</div>
	<?php endwhile; ?>
<?php endif; ?>
</div>