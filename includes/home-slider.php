<?php if(get_field('homepage_banner_toggle') == 'carousel'): ?>
<?php 
	if(have_rows('slider')): 
	$count = 0; 
	$slides = count(get_field('slider'));
?>
<div class="carousel-wrap">
    <div class="carousel slide" data-ride="carousel" data-interval="8000">
        <ol class="carousel-indicators">
            <?php for ($i = 0; $i < $slides; $i++): ?>
            <li data-target=".carousel" data-slide-to="<?php echo $i; ?>"<?php if($i == 0) echo ' class="active"'; ?>></li>
            <?php endfor; ?>
        </ol>
        <div class="carousel-inner">
			<?php while ( have_rows('slider') ) : the_row();
                if (get_sub_field('external_link')) { $link = get_sub_field('external_link'); $target="_blank"; } else { $link = get_sub_field('page_link'); $target="_self"; }
                $image = get_sub_field('image');
            ?>
            <div class="item<?php if($count == 0){ echo ' active'; } ?>">
                <a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
                  <img id="full-slider-banner" src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('heading'); ?>">
                </a>
                <div class="carousel-caption">
                    <h2><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php the_sub_field('subheading') ?></a></h2>
                    <h1><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php the_sub_field('heading') ?></a></h1>
                    <h3><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php the_sub_field('text'); ?></a></h3>
                </div>
            </div>     
            <?php $count++;endwhile; ?>
        </div>
    </div>
</div>
    <?php endif; ?>

<?php else: ?>
<div class="banner" style="background-image: url(<?php the_field('hero_image'); ?>)!important; background-repeat: no-repeat; background-position: 50%;">
	<div class="container">
		<div class="banner-txt">
		  <h2><?php the_field('title_1'); ?></h2>
		  <h1><?php the_field('title_2'); ?></h1>
		  <h3><?php the_field('description'); ?></h3>
		</div>
	</div>
</div>  
<?php endif; ?>