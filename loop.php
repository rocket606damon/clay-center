<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
		
        <h1><?php _e( 'Not Found', 'twentyten' ); ?></h1>
        
		<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyten' ); ?></p>

<?php endif; ?>

<?php
	/* Start the Loop.
	 *
	 * In Twenty Ten we use the same loop in multiple contexts.
	 * It is broken into three main parts: when we're displaying
	 * posts that are in the gallery category, when we're displaying
	 * posts in the asides category, and finally all other posts.
	 *
	 * Additionally, we sometimes check for whether we are on an
	 * archive page, a search page, etc., allowing for small differences
	 * in the loop on each template without actually duplicating
	 * the rest of the loop that is shared.
	 *
	 * Without further ado, the loop:
	 */ ?>
<?php while ( have_posts() ) : the_post(); ?>

    <div class="block cf">
		<h3><a href="<?php the_permalink(); ?>" title="Read <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
        <?php if(has_post_thumbnail()): ?>
        <div class="photo">
        	<?php the_post_thumbnail('thumbnail'); ?>
            <a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>
        </div>
        <?php endif; ?>
        <div class="text<?php if(has_post_thumbnail()) echo ' narrow'; ?>">
        	<?php if(has_category()){ ?>
            <p class="date"><?php the_time('F j, Y'); ?></p>
            <?php if(!get_field('hide_author')){ ?><p class="author">By <?php if ( function_exists( 'coauthors_posts_links' ) ) { coauthors_posts_links(); } else { the_author_posts_link(); } ?></p><?php } ?>
			<?php if ( have_comments() ) : ?>
            <p class="comments"><?php comments_popup_link( __( 'Leave a comment', 'twentyten' ), __( '1 Comment', 'twentyten' ), __( '% Comments', 'twentyten' ) ); ?></p>
			<?php endif; ?>
			<?php } ?>
			<?php print_excerpt(350); ?>
            <?php if(in_category('podcasts')): ?>
            <a class="media-cat podcasts" href="<?php bloginfo('url'); ?>/multimedia/podcasts/">Podcasts</a>
            <?php elseif(in_category('clay-center-tv')): ?>
            <a class="media-cat clay-center-tv" href="<?php bloginfo('url'); ?>/multimedia/clay-center-tv/">Clay Center TV</a>
            <?php endif; ?>
            <a class="btn" href="<?php the_permalink(); ?>" title="Read <?php the_title_attribute(); ?>">View More...</a>
            <?php $myid = $myid = get_the_ID(); $comment_count = $wpdb->get_var("SELECT comment_count FROM $wpdb->posts WHERE post_status = 'publish' AND ID = '$myid'");
			if($comment_count > 0) echo '<a class="comments" href="' . get_permalink() . '#disqus_thread">' . $comment_count . '</a>'; ?>

        </div>
    </div>
	
<?php endwhile; // End the loop. Whew. ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<?php next_posts_link( __( '&larr; Older posts', 'twentyten' ) ); ?>
				<?php previous_posts_link( __( 'Newer posts &rarr;', 'twentyten' ) ); ?>
<?php endif; ?>