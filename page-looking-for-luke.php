<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page">

    		<div class="video">
            	<?php the_field('video_link'); ?>
            </div>

            <div class="links">
                <?php the_field('left_links'); ?>
                <?php the_field('right_links'); ?>
            </div>
        
        	<p id="about-film"><strong>About the Film</strong></p>

        	<?php the_field('about_film'); ?>

        	<?php if( have_rows('filmmakers')): ?>
        		<p id="about-filmmakers" style="text-align: left;"><strong>About The Filmmakers</strong></p>

        		<?php while( have_rows('filmmakers') ): the_row(); 
        			$filmMakerPhoto = get_sub_field('picture');
        			$filmMakerPhotoSize = 'thumbnail';
        			$filmMakerPhotoThumb = $filmMakerPhoto['sizes'][$filmMakerPhotoSize];
        			$filmMakerAlt = $filmMakerPhoto['alt'];
        		?>

        		<div class="filmmaker-item">
        			<div class="photo-wrapper"><img src="<?php echo $filmMakerPhotoThumb; ?>" alt="<?php echo $filmMakerAlt; ?>" class="alignleft"></div>
        			<div class="right-text">
        				<em><?php the_sub_field('name_and_title'); ?></em>
        				<?php the_sub_field('description'); ?>
        			</div>
        		</div>

        	<?php endwhile; endif; ?>	

        	<p id="about-claycenter"><strong>About The Clay Center for Young Healthy Minds</strong></p>

        	<?php the_field('about_claycenter'); ?>

            <?php the_field('constant_contact'); ?>
        
		</div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>