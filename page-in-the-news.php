<?php get_header(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="category news-events">
        
       		<?php 
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array('post_type' => 'base_news_events', 'paged' => $paged, 'posts_per_page'=>'10' );
				$loop = new WP_Query( $args );
			?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php
				$author = get_the_author_meta('ID');
				$image = wp_get_attachment_image_src(get_user_meta($author, 'photo', true ), 'thumbnail'); 
			?>
            
            <div class="block cf">
				<?php
                    $author = get_field('author'); 
                     $image = wp_get_attachment_image_src(get_user_meta($author[ID], 'photo', true ), 'thumbnail'); 
                ?>
                <div class="photo">
                    <img src="<?php echo $image[0]; ?>" width="96" height="96" alt="<?php echo get_user_meta($author[ID], 'display_name', true ); ?>" />
                    <span></span>
                </div>
                <div class="link">
                    <?
                        $value = get_field('target');
                        if($value[0] == 'true'): 
                            $target = "_self";
                        else:
                            $target = "_blank";
                        endif;
                    ?>
                    <p class="date"><?php the_time('F j, Y'); ?></p>
                    <h3><a href="<?php the_field('url'); ?>" target="_blank"><?php the_title(); ?></a></h3>
                </div>
            </div>
			
			<?php endwhile; ?>
            <?php 
				echo get_next_posts_link('&larr; Older posts', $loop->max_num_pages);
				echo get_previous_posts_link('Newer posts &rarr;', $loop->max_num_pages);
			?>
            <?php wp_reset_query(); ?>
		
			<?php comments_template( '', true ); ?>
        
		</div><!-- .category -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>