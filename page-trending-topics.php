<?php get_header(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="category press-releases">

       		<?php query_posts('category_name=press-releases,hot-topics&showposts=10'); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php
				$author = get_the_author_meta('ID');
				$image = wp_get_attachment_image_src(get_user_meta($author, 'photo', true ), 'thumbnail'); 
			?>
            
            <div class="block cf">
            
                <div class="photo">
                    <img src="<?php echo $image[0]; ?>" width="96" height="96" alt="<?php echo get_user_meta($author[ID], 'display_name', true ); ?>" />
                    <span></span>
                </div>
                <div class="link">
                    <p class="date"><?php the_time('F j, Y'); ?></p>
                    <?php if(in_category('hot-topics')): ?>
                    <p class="cat"><a href="<?php bloginfo('url'); ?>/hot-topics/">Hot Topics</a></p>
                    <?php else: ?>
                    <p class="cat"><a href="<?php bloginfo('url'); ?>/press-releases/">Press Releases</a></p>
                    <?php endif; ?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </div>
            
            </div>
			
			<?php endwhile; ?>
            
            <?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php if (  $wp_query->max_num_pages > 1 ) : ?>
                            <?php next_posts_link( __( '&larr; Older posts', 'twentyten' ) ); ?>
                            <?php previous_posts_link( __( 'Newer posts &rarr;', 'twentyten' ) ); ?>
            <?php endif; ?>
            
            <?php endif;wp_reset_query(); ?>
		
			<?php comments_template( '', true ); ?>
        
		</div><!-- .category -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>