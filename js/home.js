$(window).load(function(){
	$('#slider').carouFredSel({
	    width: "100%",
		height: 500,
		direction: "up",
		items: {
			width: "100%",
			height: 500
		},
	    scroll: {
			fx: "crossfade"
		},
		auto: {
			pauseOnEvent: "resume",
			button: ".pause",
			timeoutDuration: 12000,
			progress: {
				bar: ".timer"
			},
			onAfter : function( data ) {
				$(this).trigger("configuration", ["items.visible", 1]);
	        }
		},
		prev: {
			button: ".prev",
			key: "left",
			onAfter : function( data ) {
				$(this).trigger("configuration", ["items.visible", 1]);
	        }
		},
		next: {
			button: ".next",
			key: "right",
			onAfter : function( data ) {
				$(this).trigger("configuration", ["items.visible", 1]);
	        }
		}
	});
	$( "#tabs" ).tabs();
});