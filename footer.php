		
        <div id="footer">
        
        	<div class="content">
            	<div class="sitemap">
                	<h2>Sitemap</h2>
            		<?php wp_nav_menu(array('theme_location' => 'primary', 'walker' => new Roots_Nav_Walker())); ?>
            	</div>
                <div class="social">
                	<h2>Social</h2>
                    <ul>
                    	<li><a class="facebook" href="http://www.facebook.com/massgeneralclaycenter" target="_blank"><i class="fa fa-facebook fa-lg fa-fw"></i>Facebook</a></li>
                        <li><a class="twitter" href="http://twitter.com/mghclaycenter" target="_blank"><i class="fa fa-twitter fa-lg fa-fw"></i>Twitter</a></li>
                        <li><a class="youtube" href="https://www.youtube.com/channel/UCBdGAoFma-eEZL4ywneftIg" target="_blank"><i class="fa fa-youtube fa-lg fa-fw"></i>YouTube</a></li>
                    </ul>
            	</div>
                <div class="info">
                	<h2>Information</h2>
                    <ul>
                    	<li>The Clay Center for Young Healthy Minds</li>
                        <?php if(get_field('footer_phone', 'option')): ?><li class="phone"><?php the_field('footer_phone', 'option'); ?></li><?php endif; ?>
                        <?php if(get_field('footer_address', 'option')): ?><li class="address"><?php the_field('footer_address', 'option'); ?><?php if(get_field('footer_map_text', 'option') && get_field('footer_map_link', 'option')): ?><br /><a href="<?php the_field('footer_map_link', 'option'); ?>" target="_blank"><?php the_field('footer_map_text', 'option'); ?></a><?php endif; ?></li><?php endif; ?>
                        <?php if(get_field('footer_email', 'option')): ?><li class="email"><a href="mailto:<?php the_field('footer_email', 'option'); ?>"><?php the_field('footer_email', 'option'); ?></a></li><?php endif; ?>
                        <?php if(get_field('footer_media_link', 'option') && get_field('footer_media_text', 'option')): ?><li class="media-footer"><i class="fa fa-newspaper-o"></i><a href="<?php the_field('footer_media_link', 'option'); ?>"><?php the_field('footer_media_text', 'option'); ?></a></li><?php endif; ?>
            		</ul>
                </div>
            </div>            
        
        </div>
        
        <div id="sub_foot">
        	<div class="content cf">
            	<a class="mgh-clay-center" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?></a>
                <a class="hms" href="http://hms.harvard.edu" target="_blank">Harvard Medical School</a>
                <a class="mgh" href="http://www.massgeneral.org" target="_blank">Massachusetts General Hospital</a>
            </div>
            <div id="copyright">&copy; <?php echo date("Y") ?> The Clay Center for Young Healthy Minds, Massachusetts General Hospital<br />The Clay Center for Young Healthy Minds at Massachusetts General Hospital is a non-profit 501(c)(3) organization.</div>
        </div>
        
        
            
    </div><!-- #wrap -->
     
<?php wp_footer(); ?>

<div id="ttdUniversalPixelTagad20a34cd1c94cd198ff7062c837fc56" style="display:none">
  <script src="https://js.adsrvr.org/up_loader.1.1.0.js" type="text/javascript"></script>
  <script type="text/javascript">
	(function(global) {
	  if (typeof TTDUniversalPixelApi === 'function') {
		var universalPixelApi = new TTDUniversalPixelApi();
		 universalPixelApi.init("oew66ml", ["rvr98z3"], "https://insight.adsrvr.org/track/up", "ttdUniversalPixelTagad20a34cd1c94cd198ff7062c837fc56");
	  }
	})(this);
  </script>
</div>
</body>
</html>