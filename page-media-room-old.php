<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page">
        	
            <?php the_content(); ?>
            
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#press-releases" data-toggle="tab"><i class="fa fa-newspaper-o"></i> Press Releases</a></li>
                    <li><a href="#white-papers" data-toggle="tab"><i class="fa fa-file-text-o"></i> White Papers</a></li>
                    <li><a href="#podcasts" data-toggle="tab"><i class="fa fa-volume-up"></i> Podcasts</a></li>
                    <li><a href="#videos" data-toggle="tab"><i class="fa fa-video-camera"></i> Videos</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="press-releases">
                    
                    <?php
						if( have_rows('press_release') ):
						while ( have_rows('press_release') ) : the_row();
					?>
                    	<div class="file">
                            <p class="date"><?php the_sub_field('date'); ?></p>
                            <h3><i class="fa fa-file-pdf-o"></i><a href="<?php the_sub_field('file'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></h3>
                    	</div>
					<?php endwhile;endif; ?>
						
                    </div>
                    <div class="tab-pane" id="white-papers">

                    </div>
                    <div class="tab-pane" id="podcasts">

                    </div>
                    <div class="tab-pane" id="videos">

                    </div>
                </div>

				<script type="text/javascript">
					$( document ).ready(function() {
						 $('#tabs').tab();
					});
 				</script>    
            
			<?php comments_template( '', true ); ?>
        
		</div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>