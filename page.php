<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page">
        	
            <?php if(has_post_thumbnail() && $post->post_parent != '5623'): ?>
            <div id="photo">
            	<?php the_post_thumbnail('article-header'); ?>
                <?php 
					if(get_field('author_link')):
                    $author = get_field('author_link');
                    $user_object = get_userdata($author[ID]);
                ?>
                 <ul>
                    <li><a class="email" href="mailto:<?php echo $user_object->user_email; ?>">Email</a></li>
                    <?php if(get_user_meta($author[ID], 'twitter', true ) != '') { ?><li><a class="twitter" href="http://twitter.com/<?php echo get_user_meta($author[ID], 'twitter', true ); ?>" target="_blank">Twitter</a></li><?php } ?>
                 </ul>
                 <?php endif; ?>
            </div>
            <?php endif; ?>
			<?php 
				if(get_field('featured_headshot')): 
				$image = get_field('featured_headshot');
				$size = 'media-downloads';
				$thumb = $image['sizes'][ $size ];
			?>
            <img class="featured-headshot" src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
            <?php endif; ?>
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>
            
            <?php 
				if( $post->post_parent == '74' || $post->post_parent == '5623' ) :
					$author = get_field('author_link');
					$user_object = get_userdata($author[ID]);
			?>
            <a class="btn right" href="<?php bloginfo('url'); ?>/author/<?php echo $user_object->user_nicename; ?>">View All Articles By <?php echo $user_object->user_firstname; ?></a>
            <?php endif; ?>

            <?php if( have_rows('photos') ): ?>
            <div class="media-photos">
                <h3>Photos</h3>
                <ul>
                <?php 
                  while ( have_rows('photos') ) : the_row();
                  $image = get_sub_field('photo');
                  $image_url = $image['url'];
                  $size = 'media-downloads';
                  $thumb = $image['sizes'][ $size ];
                ?>
                
                <li><a href="<?php echo $image_url; ?>" target="_blank"><img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" width="200" height="200" /></a></li>
            
                <?php endwhile; ?>
                </ul>
			</div>
			<?php endif; ?>
            
            <?php if( have_rows('files') ): ?>
            <div class="media-downloads">
                <h3>File Downloads</h3>
                <ul>
                <?php 
                  while ( have_rows('files') ) : the_row();
				  $file = get_sub_field('file');
                ?>
                
                <li><a href="<?php echo $file['url']; ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
            
                <?php endwhile; ?>
                </ul>
			</div>
			<?php endif; ?>
			
		
			<?php comments_template( '', true ); ?>
        
		</div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>