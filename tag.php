<?php
/**
 * The template for displaying Tag Archive pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title">Articles containing: <?php single_tag_title(); ?></h3>
				        
        <div class="category">

			<?php get_template_part( 'loop', 'category' ); ?>

		</div><!-- .category -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php get_footer(); ?>