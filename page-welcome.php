<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page">
        
        	<?php the_content(); ?>
        	
        	<ul class="menu">
				<?php 
                    $mypages = get_pages(array('child_of' => wp_get_post_parent_id($post->ID),'parent' => wp_get_post_parent_id($post->ID), 'exclude' => $post->ID, 'sort_column' => 'menu_order'));
                    foreach( $mypages as $page ):	
                ?>
                <li>
                	<h3><?php echo $page->post_title; ?></h3>
                	<a class="btn" href="<?php echo get_page_link( $page->ID ); ?>">View</a>
                </li>
				<?php endforeach;	?>
            </ul>
            
            <?php the_field('contact_info'); ?>
            
			<?php comments_template( '', true ); ?>
        
		</div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>