<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="content cf">

	<div id="main">
    
    	<h3 class="title"><?php the_title(); ?></h3>
    
    	<div class="page">
        
        	<?php the_content(); ?>
            
            <div class="content-block about-the-clay-center">
            	<h2><a href="<?php bloginfo('url'); ?>/media-center/about-the-clay-center/">About The Clay Center</a></h2>
                <a class="btn more" href="<?php bloginfo('url'); ?>/media-center/about-the-clay-center/">More</a>
                <ul>
                	<li>
                    	<h3>Our Mission Statement</h3>
                        <ul>
                    		<li><a class="btn view" href="<?php bloginfo('url'); ?>/media-room/about-the-clay-center/#our-mission-statement"><i class="fa fa-eye"></i> View</a></li>
                        	<li><a class="btn download" href="<?php the_field('mission_statement'); ?>" target="_blank"><i class="fa fa-download"></i> Download</a></li>
                        </ul>
                    </li>
                    <li>
                    	<h3>Our Brand Story</h3>
                        <ul>
                    		<li><a class="btn view" href="<?php bloginfo('url'); ?>/media-room/about-the-clay-center/#our-brand-story"><i class="fa fa-eye"></i> View</a></li>
                        	<li><a class="btn download" href="<?php the_field('brand_story'); ?>" target="_blank"><i class="fa fa-download"></i> Download</a></li>
                        </ul>
                    </li>
                    <li>
                    	<h3>Our Boilerplate</h3>
                        <ul>
                    		<li><a class="btn view" href="<?php bloginfo('url'); ?>/media-room/about-the-clay-center/#our-boilerplate"><i class="fa fa-eye"></i> View</a></li>
                        	<li><a class="btn download" href="<?php the_field('boilerplate'); ?>" target="_blank"><i class="fa fa-download"></i> Download</a></li>
                        </ul>
                    </li>
                    <li>
                    	<h3>Why We Exist</h3>
                        <ul>
                    		<li><a class="btn view" href="<?php bloginfo('url'); ?>/media-room/about-the-clay-center/#why-we-exist"><i class="fa fa-eye"></i> View</a></li>
                        	<li><a class="btn download" href="<?php the_field('why_we_exist'); ?>" target="_blank"><i class="fa fa-download"></i> Download</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            
            <div class="content-block our-doctors">
            	<h2><a href="<?php bloginfo('url'); ?>/media-room/our-team/">Our Team</a></h2>
                <a class="btn more" href="<?php bloginfo('url'); ?>/media-room/our-team/">More</a>
                <ul>
				<?php
					$mypages = get_pages( array( 'child_of' => 5623, 'sort_column' => 'menu_order') );
					foreach( $mypages as $page ) {		
					$content = $page->post_content;
					$content = apply_filters( 'the_content', $content ); 
					$excerpt = substr($content,0, strpos($content, "</p>")+4);
					$author = get_field('author_link', $page->ID);
					$user_object = get_userdata($author[ID]);
					$titles = get_user_meta($author[ID], 'titles', true );
					$email = $user_object->user_email;
				?>
				<li>
                	<h3><?php echo $page->post_title; ?><a class="email" href="mailto:<?php echo $user_object->user_email; ?>"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></a><?php if(get_user_meta($author[ID], 'twitter', true ) != '') { ?><a class="twitter" href="http://twitter.com/<?php echo get_user_meta($author[ID], 'twitter', true ); ?>" target="_blank"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></a><?php } ?></h3>
                    <ul>
                    	<li><a class="btn view" href="<?php echo get_page_link( $page->ID ); ?>"><i class="fa fa-eye"></i> View</a></li>
                        <li><a class="btn download" href="<?php the_field('bio_download', $page->ID); ?>" target="_blank"><i class="fa fa-download"></i> Download</a></li>
                    </ul>
                </li>
				<?php }	?>
                </ul>
            </div>
            
            <div class="content-block press-releases">
				<h2><a href="<?php bloginfo('url'); ?>/media-center/trending-topics/">Trending Topics</a></h2>
				<a class="btn more" href="<?php bloginfo('url'); ?>/media-center/trending-topics/">More</a>
                <ul>
				<?php query_posts('category_name=press-releases,hot-topics&showposts=5'); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php
                    $author = get_the_author_meta('ID');
                    $image = wp_get_attachment_image_src(get_user_meta($author, 'photo', true ), 'thumbnail'); 
                ?>
                <li>
                  <p class="date"><?php the_time('F j, Y'); ?></p>
                  <?php if(in_category('hot-topics')): ?>
                  <p class="cat"><a href="<?php bloginfo('url'); ?>/hot-topics/">Hot Topics</a></p>
                  <?php else: ?>
                  <p class="cat"><a href="<?php bloginfo('url'); ?>/press-releases/">Press Releases</a></p>
                  <?php endif; ?>
                  <p><a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a></p>
                  
                </li>
                <?php endwhile;endif;wp_reset_query(); ?>
                </ul>
            </div>
            
            <div class="content-block videos-photos">
            	<h2><a href="<?php bloginfo('url'); ?>/media-center/videos/">Videos</a></h2>
                <a class="btn more" href="<?php bloginfo('url'); ?>/media-center/videos/">More</a>
            </div>
            
            <div class="content-block testimonials">
            	<h2><a href="<?php bloginfo('url'); ?>/media-center/testimonials/">Testimonials</a></h2>
            	<a class="btn more" href="<?php bloginfo('url'); ?>/media-center/testimonials/">More</a>
            </div>
            
            <?php the_field('contact_info'); ?>
            
			<?php comments_template( '', true ); ?>
        
		</div><!-- .page -->
    
    </div><!-- #main -->
    
    <?php include( TEMPLATEPATH . '/includes/sidebar.php'); ?>

</div><!-- .content -->

<?php endwhile; ?>

<?php get_footer(); ?>