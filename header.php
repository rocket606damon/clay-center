<?php include( TEMPLATEPATH . '/includes/classes-titles.php'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php if(is_author()): ?>
<title><?php global $wp_query; $curauth = $wp_query->get_queried_object(); echo 'Articles by ' . $curauth->display_name . ' | ' . get_bloginfo('name'); ?></title>
<?php else: ?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php endif; ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<!-- <?php //if(is_front_page()): ?><link rel="stylesheet" type="text/css" media="all" href="<?php //echo home_url() . '/js/bootstrap.min.css'; ?>?<?php //echo date("His"); ?>" /><?php //endif; ?> -->
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?<?php echo date("His"); ?>" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script src="//use.typekit.net/phn3hhv.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<?php wp_head(); ?>
<?php if(is_page('home')) { include( TEMPLATEPATH . '/includes/home-head.php'); } ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0010/4582.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44169315-1', 'mghclaycenter.org');
  ga('send', 'pageview');
</script>
</head>

<body class="<?php echo $page_class; ?>">

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

	<div id="wrapper">

    	<div id="header">
        
          <!--
          <div class="top-bar cf">
            <div class="content cf">
              
              <div class="logo-band">
                <a class="hms" href="http://hms.harvard.edu" target="_blank">Harvard Medical School</a>
                <a class="mgh" href="http://www.massgeneral.org" target="_blank">Massachusetts General Hospital</a>
              </div>
              
              <ul class="social">
                <li><a class="facebook" href="http://www.facebook.com/massgeneralclaycenter" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a></li>
                <li><a class="twitter" href="http://twitter.com/mghclaycenter" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></a></li>
                <li><a class="youtube" href="https://www.youtube.com/channel/UCBdGAoFma-eEZL4ywneftIg" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i></span></a></li>
             </ul>
           </div>
          </div>
          -->
          
        	<div class="content cf">
            <div class="header-left cf">
              <h1><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?></a></h1>
            </div>
            <div class="header-right cf">
              <div id="top-menu cf">
                <ul class="sub-menu">
                  <li class="contact"><i class="fa fa-envelope-o"></i><a href="<?php bloginfo('url'); ?>/about-us/contact-information/">Contact Us</a></li>
                  <li class="donate"><a href="http://www.mghclaycenter.org/your-support/" target="_self">Donate Now</a></li>
                  <li class="search"><?php get_search_form(); ?></li>
                </ul>
              </div>
              <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'menu cf', 'walker' => new Roots_Nav_Walker())); ?>
            </div>
          </div>

        </div><!-- #header -->

        <div id="banner">
      <?php if(is_page('looking-for-luke')) echo "<a href='http://lookingforlukefilm.com' target='_blank'>"; ?>
			<?php if(is_page('home')) { include( TEMPLATEPATH . '/includes/home-slider.php'); } else { ?>
            <div class="content">
            	<div class="internal">
                	<?php
					$category = get_the_category();
					$category = $category[0]->slug;
					if($category == 'press-releases'){
						$category_title = 'Clay Center Media Room';
						$category_desc = 'Explore The Clay Center’s Brand Story, Mission Statement, Boilerplate and more.';
					}
					?>
                    <h1><?php echo $category_title; ?></h1>
                    <p><?php echo $category_desc; ?></p>
            	</div>
            </div>
			<?php } ?>
      <?php if(is_page('looking-for-luke')) echo "</a>"; ?>
        </div>